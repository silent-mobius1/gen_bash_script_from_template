#!/usr/bin/env bash
#set -x
##############################
#created by : Br0k3ngl255
#version: 0.0.1
#purpose: generate from bash template
#date : 12/30/2018
##############################

##variables ++++++++++++++++++++++++++++++++++++++++++++++++++++
script_name="$5"
shabang="#!/usr/bin/env bash"
line="#########################################################"
comment="#"
my_date=$1
my_purpose=$2
my_version=$3
my_author=$4
my_time="2.5"

#msg variable ::::::::::::::::::::::::::::::::::::::::::::::::
msg_start="Starting Script..."
msg_end="Script finished"


####
#Main  - _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _
###


echo $line
echo $msg_start
echo $line

sleep $my_time

if [ -z $my_date ];then
	echo "no date"
	exit 1
fi

if [ -z $my_purpose ];then
	echo "no purpose"
	exit 1
fi

if [ -z $my_version ];then
	echo "no version"
	exit 1
fi

if [ -z $my_author ];then
	echo "no author"
	exit 1
fi

if [ -z $script_name ];then
	echo "no script name"
	exit 1
fi



echo $shabang > $script_name
echo $line    >> $script_name
echo "$comment $my_author" >>  $script_name
echo "$comment $my_date "  >>  $script_name
echo "$comment $my_version" >> $script_name
echo "$comment $my_purpose" >> $script_name
echo $line    >> $script_name


sleep $my_time

echo $line
echo $msg_end
echo $line


